
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * TPL Variables

INTRODUCTION
------------

Current Maintainer: Benjamin HUBERT <benjamin@alpixel.fr>

Wunderground is a module that provides a new block you can configure in back
office. This block fetch weather informations from wunderground database.
You have to configure the module to specify the country and city you want to
fetch the informations for.


INSTALLATION
------------

Just enable the Wunderground module and configure it in the services panel

TPL Variables
--------------

In the block's TPL, you can use the following variable :

Current condition :
      $current['status'] : Short (one word) description of the current weather

      $current['icon'] : Word representing the current weather (file ready) -
                         Use this word to display your own images
      $current['icon_uri'] : URL of the icon
          @see http://www.wunderground.com/weather/api/d/docs?d=resources/phrase
          -glossary#current_condition_phrases

      $current['wind_degrees']
      $current['wind_dir']
      $current['wind_speed'] : Speed in MPH or KMH (configure it in the admin)
      $current['temperature'] : Temp in Fahrenheit or Celsius
                                (configure it in the admin)
      $current['temperature_feelslike']
      $current['humidity']
      $current['uv']

      Forecast :
      $forecast['status'] : Short (one word) description of the weather

      $forecast['icon'] : Word representing the weather (file ready) -
                          Use this word to display your own images
      $forecast['icon_uri'] : URL of the icon
          @see http://www.wunderground.com/weather/api/d/docs?d=resources/phrase
          -glossary#current_condition_phrases
      $forecast['date'] : Date object sent by the API
            Example :
               epoch: "1346360400",
                pretty: "11:00 PM CEST on August 30, 2012",
                day: 30,
                month: 8,
                year: 2012,
                yday: 242,
                hour: 23,
                min: "00",
                sec: 0,
                isdst: "1",
                monthname: "aout",
                weekday_short: "jeu",
                weekday: "jeudi",
                ampm: "PM",
                tz_short: "CEST",
                tz_long: "Europe/Paris"
      $forecast['temperature'] : Array of two items (high and low)
      $forecast['humidity']
