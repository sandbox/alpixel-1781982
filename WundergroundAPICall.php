<?php
/**
 * @file
 * Main Class for calling the wunderground API.
 */

class WundergroundAPICall {

  protected $apikey;
  protected $language;

  const API_URI = "http://api.wunderground.com/api/";

  /**
   * Constructor.
   * @params String $apikey
   * @params String $language
   */
  public function __construct($apikey, $language) {
    $this->apikey = $apikey;
    $this->language = $language;
  }

  /**
   * Internal function used to call the api.
   */
  protected function call($method, $city, $country) {
    $cache = $this->cacheGet($this->cacheGetID($method));
    if (empty($cache)) {
      if (empty($method) || empty($city) || empty($country) || empty($this->apikey)) {
        drupal_set_message(t('Wunderground has been to configured. Please go to the admin panel :') . ' admin/config/services/wunderground_api', 'error');
        watchdog("wunderground", "Missing essential parameter", array(), WATCHDOG_CRITICAL);
        return FALSE;
      }

      $uri = self::API_URI . $this->apikey . "/" . $method . "/lang:" . drupal_strtoupper($this->language) . "/q/" . rawurlencode($country) . "/" . rawurlencode($city) . ".json";
      watchdog("wunderground", "Weather call : " . $method, array(), WATCHDOG_INFO, $uri);
      $request = drupal_http_request($uri);

      $data = isset($request->data) ? $request->data : NULL;
      if (mb_detect_encoding($data, 'UTF-8', TRUE) !== 'UTF-8') {
        $data = utf8_encode($data);
      }

      $response = @json_decode($data);

      if (!empty($response) && !empty($response->response)) {
        if (!empty($response->response->error)) {
          watchdog("wunderground", "Error of type @type detected : @message", array(
            "@type"    => $response->response->error->type,
            "@message" => $response->response->error->description,
            ), WATCHDOG_ERROR, $uri);
        }
        $this->cacheSet($this->cacheGetID($method), $response, variable_get('wunderground_cache_lifetime', 12) * 3600);
        return $response;
      }
      else {
        watchdog("wunderground", "Can't parse response. Please click on the link to see if API is available", array(), WATCHDOG_ERROR, $uri);
      }
      return FALSE;
    }
    else {
      return $cache;
    }
  }

  /**
   * Public method to call the current weather method.
   */
  public function getCurrentWeather($city, $country, $original_object=FALSE) {
    $response = $this->call('conditions', $city, $country);
    if (!empty($response)) {
      if ($original_object) {
        return $response;
      }
      else {
        $response_array = array();
        $response_array['status'] = $response->current_observation->weather;
        $response_array['icon'] = $response->current_observation->icon;
        $response_array['icon_uri'] = $response->current_observation->icon_url;

        $response_array['wind_degrees'] = $response->current_observation->wind_degrees;
        $response_array['wind_dir'] = $response->current_observation->wind_dir;
        if (variable_get('wunderground_wind_unit', 'kmh') == "kmh") {
          $response_array['wind_speed'] = $response->current_observation->wind_kph;
        }
        else {
          $response_array['wind_speed'] = $response->current_observation->wind_mph;
        }

        if (variable_get('wunderground_temperature_unit', 'c') == "c") {
          $response_array['temperature'] = $response->current_observation->temp_c;
          $response_array['temperature_feelslike'] = $response->current_observation->feelslike_c;
        }
        else {
          $response_array['temperature'] = $response->current_observation->temp_f;
          $response_array['temperature_feelslike'] = $response->current_observation->feelslike_f;
        }

        $response_array['humidity'] = $response->current_observation->relative_humidity;
        $response_array['uv'] = $response->current_observation->UV;

        return $response_array;
      }
    }
    return FALSE;
  }

  /**
   * Public method to forecast for the next few days.
   */
  public function getForecast($city, $country, $days=3, $original_object=FALSE) {
    $response = $this->call('forecast', $city, $country);
    if (!empty($response)) {
      if ($original_object) {
        return $response;
      }
      else {
        $response_array = array();
        for ($i = 0; $i < $days; $i++) {
          if (!empty($response->forecast->simpleforecast->forecastday[$i])) {
            $day = $response->forecast->simpleforecast->forecastday[$i];
            $new_day = array();
            $new_day['status'] = $day->conditions;
            $new_day['icon'] = $day->icon;
            $new_day['icon_uri'] = $day->icon_url;
            $new_day['date'] = $day->date;

            if (variable_get('wunderground_temperature_unit', 'c') == "c") {
              $new_day['temperature']['high'] = $day->high->celsius;
              $new_day['temperature']['low'] = $day->low->celsius;
            }
            else {
              $new_day['temperature']['high'] = $day->high->fahrenheit;
              $new_day['temperature']['low'] = $day->low->fahrenheit;
            }

            $new_day['humidity'] = $day->avehumidity;

            $response_array[] = $new_day;
          }
        }
        return $response_array;
      }
    }
    return FALSE;
  }

  /**
   * Get cache ID for a given method.
   */
  protected function cacheGetID($method) {
    return "wunderground_api_block_" . $method . "_" . $this->language;
  }

  /**
   * Retrieve cache infos.
   */
  protected function cacheGet($id) {
    $cache = cache_get($id, 'cache_block');
    if (!empty($cache)) {
      return $cache->data;
    }
    return FALSE;
  }

  /**
   * Set cache info.
   */
  protected function cacheSet($id, $data, $lifetime) {
    return cache_set($id, $data, 'cache_block', time() + $lifetime);
  }


}
