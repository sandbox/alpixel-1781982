<?php

/**
 * @file
 * Template for wunderground block.
 *
 * Current condition :
 * - $weather_data['current']['status'] : Short (one word) description of
 * the current weather
 *
 * - $weather_data['current']['icon'] : Word representing the current
 * weather (file ready) - Use this word to display your own images
 * - $weather_data['current']['icon_uri'] : URL of the icon
 *     @link http://www.wunderground.com/weather/api/d/docs?d=resources/phr
 * ase-glossary#current_condition_phrases
 *
 * - $weather_data['current']['wind_degrees']
 * - $weather_data['current']['wind_dir']
 * - $weather_data['current']['wind_speed'] : Speed in MPH or KMH
 * - $weather_data['current']['temperature'] : Temp in F° or C°
 * - $weather_data['current']['temperature_feelslike']
 * - $weather_data['current']['humidity']
 * - $weather_data['current']['uv']
 *
 * Forecast :
 * - $weather_data['forecast']['status'] : Short (one word) description of
 * the weather
 *
 * - $weather_data['forecast']['icon'] : Word representing the weather
 * (file ready) - Use this word to display your own images
 * - $weather_data['forecast']['icon_uri'] : URL of the icon
 *     @link http://www.wunderground.com/weather/api/d/docs?d=resources/phr
 * ase-glossary#current_condition_phrases
 * - $weather_data['forecast']['date'] : Date object sent by the API
 *       Example :
 *          epoch: "1346360400",
 *           pretty: "11:00 PM CEST on August 30, 2012",
 *           day: 30,
 *           month: 8,
 *           year: 2012,
 *           yday: 242,
 *           hour: 23,
 *           min: "00",
 *           sec: 0,
 *           isdst: "1",
 *           monthname: "août",
 *           weekday_short: "jeu",
 *           weekday: "jeudi",
 *           ampm: "PM",
 *           tz_short: "CEST",
 *           tz_long: "Europe/Paris"
 * - $weather_data['forecast']['temperature'] : Array of two items (high
 * and low)
 * - $weather_data['forecast']['humidity']
 */

?>
<div class="<?php print $classes; ?> weather_forecast">
  <div class="general_information">
      <h4 class='location'></h4>
  </div>
  <div class="current_weather">
      <h5 class='day'><?php print t('Now'); ?></h5>
      <img src='<?php print $weather_data['current']['icon_uri']; ?>' alt='<?php
      print $weather_data['current']['status']; ?>' />
      <dl>
		  <dt><?php print t('Condition'); ?> : </dt>
          <dd><?php print $weather_data['current']['status']; ?></dd>
          <dt><?php print t('Temperature'); ?> : </dt>
          <dd><?php print $weather_data['current']['temperature']; ?></dd>
	      <dt><?php print t('Wind'); ?> : </dt>
	      <dd><?php print $weather_data['current']['wind_speed']
         . " " . t("km/h") . $weather_data['current']['wind_dir']; ?></dd>
      </dl>
	  <div class="clearfix"></div>
  </div>
  <div class="forecast">
      <?php foreach($weather_data['forecast'] as $day): ?>
      <div class="day-container">
          <h5 class='day'><?php print $day['date']->weekday; ?></h5>
          <img src='<?php print $day['icon_uri']; ?>' alt='<?php
          print $day['status']; ?>' />
          <dl>
			  <dt><?php print t('Condition'); ?> : </dt>
	          <dd><?php print $day['status']; ?></dd>
              <dt><?php print t('Highest temperature'); ?> : </dt>
              <dd><?php print $day['temperature']['high']; ?>°C</dd>
	          <dt><?php print t('Lowest temperature'); ?> : </dt>
	          <dd><?php print $day['temperature']['low']; ?>°C</dd>
          </dl>
      </div>
	  <div class="clearfix"></div>
      <?php endforeach; ?>
  </div>
</div>
